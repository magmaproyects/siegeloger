﻿using MagmaFlyffSiegeReader.Models;
using MagmaFlyffSiegeReader.Reader;
using MagmaFlyffSiegeReader.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MagmaFlyffSiegeReader.Forms
{
    public partial class MainForm : Form
    {
        private IEnumerable<Guild> guilds;
        private OpenFileDialog openFileLog;
        private bool isHeroTypeSiege;
        private Image crown;
        private Image medal;
        public MainForm()
        {
            InitializeComponent();
            openFileLog = new OpenFileDialog();
            guilds = new HashSet<Guild>();
            isHeroTypeSiege = chboxHeroType.Checked;
            medal = ResourceImages.Medal;
            crown = ResourceImages.Crown;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileLog.ShowDialog() != DialogResult.OK)
                return;

            LogReader lr = new LogReader(openFileLog.FileName);
            guilds = lr.Siegeboard().OrderByDescending(guild => guild.Points).ToList();
            guildBindingSource.DataSource = guilds;
            setGuildOrders();
            var players = guilds.Select(guild => guild.Players);
            List<Player> play = new List<Player>();
            foreach (var player in players)
            {
                play.AddRange(player);
            }

            playerBindingSource.DataSource = play.OrderByDescending(player => player.PointsRp).ToList();
            setPlayersOrders();
        }
        private void setGuildOrders()
        {

            int order = 1;

            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                if (order == 1)
                {
                    ((TextAndImageCell)row.Cells["index"]).Image = crown;
                }


                row.Cells["index"].Value = order;
                order++;
            }
        }
        private void setPlayersOrders()
        {
            int order = 1;

            foreach (DataGridViewRow row in dataGridView2.Rows)
            {
                if (order == 1)
                {
                    ((TextAndImageCell)row.Cells["playersIndex"]).Image = medal;
                }

                row.Cells["playersIndex"].Value = order;

                order++;
            }
        }

        private void chboxHeroType_CheckedChanged(object sender, EventArgs e)
        {
            isHeroTypeSiege = chboxHeroType.Checked;
            if (isHeroTypeSiege)
            {
                medal = ResourceImages.HeroMedal;
                crown = ResourceImages.HeroCrown;
            }
            else
            {
                medal = ResourceImages.Medal;
                crown = ResourceImages.Crown;
            }

        }
    }
}
