﻿namespace MagmaFlyffSiegeReader.Forms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.chboxHeroType = new System.Windows.Forms.CheckBox();
            this.textAndImageColumn1 = new MagmaFlyffSiegeReader.Utils.TextAndImageColumn();
            this.textAndImageColumn2 = new MagmaFlyffSiegeReader.Utils.TextAndImageColumn();
            this.playerBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.Index = new MagmaFlyffSiegeReader.Utils.TextAndImageColumn();
            this.nameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.killsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pointsWithRPDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pointsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.playerNameListDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.guildBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.playersIndex = new MagmaFlyffSiegeReader.Utils.TextAndImageColumn();
            this.nameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GuildName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PointsRp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pointsDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.killsDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Deaths = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.KD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.guildBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(747, 37);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(116, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Browse File";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToOrderColumns = true;
            this.dataGridView1.AutoGenerateColumns = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Index,
            this.nameDataGridViewTextBoxColumn,
            this.killsDataGridViewTextBoxColumn,
            this.pointsWithRPDataGridViewTextBoxColumn,
            this.pointsDataGridViewTextBoxColumn,
            this.playerNameListDataGridViewTextBoxColumn});
            this.dataGridView1.DataSource = this.guildBindingSource;
            this.dataGridView1.Location = new System.Drawing.Point(12, 95);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(851, 150);
            this.dataGridView1.TabIndex = 1;
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.AllowUserToOrderColumns = true;
            this.dataGridView2.AutoGenerateColumns = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.playersIndex,
            this.nameDataGridViewTextBoxColumn1,
            this.GuildName,
            this.PointsRp,
            this.pointsDataGridViewTextBoxColumn1,
            this.killsDataGridViewTextBoxColumn1,
            this.Deaths,
            this.KD});
            this.dataGridView2.DataSource = this.playerBindingSource;
            this.dataGridView2.Location = new System.Drawing.Point(12, 287);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.Size = new System.Drawing.Size(851, 150);
            this.dataGridView2.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(363, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Guild Leaderboard";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(363, 260);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Player Leaderboard";
            // 
            // chboxHeroType
            // 
            this.chboxHeroType.AutoSize = true;
            this.chboxHeroType.Location = new System.Drawing.Point(653, 42);
            this.chboxHeroType.Name = "chboxHeroType";
            this.chboxHeroType.Size = new System.Drawing.Size(79, 17);
            this.chboxHeroType.TabIndex = 5;
            this.chboxHeroType.Text = "Hero Siege";
            this.chboxHeroType.UseVisualStyleBackColor = true;
            this.chboxHeroType.CheckedChanged += new System.EventHandler(this.chboxHeroType_CheckedChanged);
            // 
            // textAndImageColumn1
            // 
            this.textAndImageColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.textAndImageColumn1.HeaderText = "#";
            this.textAndImageColumn1.Image = null;
            this.textAndImageColumn1.Name = "textAndImageColumn1";
            this.textAndImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.textAndImageColumn1.Width = 39;
            // 
            // textAndImageColumn2
            // 
            this.textAndImageColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.textAndImageColumn2.FillWeight = 111.1675F;
            this.textAndImageColumn2.HeaderText = "#";
            this.textAndImageColumn2.Image = null;
            this.textAndImageColumn2.Name = "textAndImageColumn2";
            this.textAndImageColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // playerBindingSource
            // 
            this.playerBindingSource.DataSource = typeof(MagmaFlyffSiegeReader.Models.Player);
            // 
            // Index
            // 
            this.Index.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.ColumnHeader;
            this.Index.HeaderText = "#";
            this.Index.Image = null;
            this.Index.Name = "Index";
            this.Index.ReadOnly = true;
            this.Index.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Index.Width = 39;
            // 
            // nameDataGridViewTextBoxColumn
            // 
            this.nameDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nameDataGridViewTextBoxColumn.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn.HeaderText = "Guild";
            this.nameDataGridViewTextBoxColumn.Name = "nameDataGridViewTextBoxColumn";
            this.nameDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // killsDataGridViewTextBoxColumn
            // 
            this.killsDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.killsDataGridViewTextBoxColumn.DataPropertyName = "Kills";
            this.killsDataGridViewTextBoxColumn.HeaderText = "Kills";
            this.killsDataGridViewTextBoxColumn.Name = "killsDataGridViewTextBoxColumn";
            this.killsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // pointsWithRPDataGridViewTextBoxColumn
            // 
            this.pointsWithRPDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.pointsWithRPDataGridViewTextBoxColumn.DataPropertyName = "PointsWithRP";
            this.pointsWithRPDataGridViewTextBoxColumn.HeaderText = "Points(WithRP)";
            this.pointsWithRPDataGridViewTextBoxColumn.Name = "pointsWithRPDataGridViewTextBoxColumn";
            this.pointsWithRPDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // pointsDataGridViewTextBoxColumn
            // 
            this.pointsDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.pointsDataGridViewTextBoxColumn.DataPropertyName = "Points";
            this.pointsDataGridViewTextBoxColumn.HeaderText = "Points";
            this.pointsDataGridViewTextBoxColumn.Name = "pointsDataGridViewTextBoxColumn";
            this.pointsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // playerNameListDataGridViewTextBoxColumn
            // 
            this.playerNameListDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.playerNameListDataGridViewTextBoxColumn.DataPropertyName = "PlayerNameList";
            this.playerNameListDataGridViewTextBoxColumn.HeaderText = "Players List";
            this.playerNameListDataGridViewTextBoxColumn.Name = "playerNameListDataGridViewTextBoxColumn";
            this.playerNameListDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // guildBindingSource
            // 
            this.guildBindingSource.DataSource = typeof(MagmaFlyffSiegeReader.Models.Guild);
            // 
            // playersIndex
            // 
            this.playersIndex.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.playersIndex.FillWeight = 37.05584F;
            this.playersIndex.HeaderText = "#";
            this.playersIndex.Image = null;
            this.playersIndex.Name = "playersIndex";
            this.playersIndex.ReadOnly = true;
            this.playersIndex.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // nameDataGridViewTextBoxColumn1
            // 
            this.nameDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nameDataGridViewTextBoxColumn1.DataPropertyName = "Name";
            this.nameDataGridViewTextBoxColumn1.FillWeight = 98.99202F;
            this.nameDataGridViewTextBoxColumn1.HeaderText = "Name";
            this.nameDataGridViewTextBoxColumn1.Name = "nameDataGridViewTextBoxColumn1";
            this.nameDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // GuildName
            // 
            this.GuildName.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.GuildName.DataPropertyName = "GuildName";
            this.GuildName.FillWeight = 98.99202F;
            this.GuildName.HeaderText = "GuildName";
            this.GuildName.Name = "GuildName";
            this.GuildName.ReadOnly = true;
            // 
            // PointsRp
            // 
            this.PointsRp.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.PointsRp.DataPropertyName = "PointsRp";
            this.PointsRp.FillWeight = 98.99202F;
            this.PointsRp.HeaderText = "PointsRp";
            this.PointsRp.Name = "PointsRp";
            this.PointsRp.ReadOnly = true;
            // 
            // pointsDataGridViewTextBoxColumn1
            // 
            this.pointsDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.pointsDataGridViewTextBoxColumn1.DataPropertyName = "Points";
            this.pointsDataGridViewTextBoxColumn1.FillWeight = 98.99202F;
            this.pointsDataGridViewTextBoxColumn1.HeaderText = "Points";
            this.pointsDataGridViewTextBoxColumn1.Name = "pointsDataGridViewTextBoxColumn1";
            this.pointsDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // killsDataGridViewTextBoxColumn1
            // 
            this.killsDataGridViewTextBoxColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.killsDataGridViewTextBoxColumn1.DataPropertyName = "Kills";
            this.killsDataGridViewTextBoxColumn1.FillWeight = 98.99202F;
            this.killsDataGridViewTextBoxColumn1.HeaderText = "Kills";
            this.killsDataGridViewTextBoxColumn1.Name = "killsDataGridViewTextBoxColumn1";
            this.killsDataGridViewTextBoxColumn1.ReadOnly = true;
            // 
            // Deaths
            // 
            this.Deaths.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Deaths.DataPropertyName = "Deaths";
            this.Deaths.FillWeight = 98.99202F;
            this.Deaths.HeaderText = "Deaths";
            this.Deaths.Name = "Deaths";
            this.Deaths.ReadOnly = true;
            // 
            // KD
            // 
            this.KD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.KD.DataPropertyName = "KD";
            this.KD.FillWeight = 98.99202F;
            this.KD.HeaderText = "KD";
            this.KD.Name = "KD";
            this.KD.ReadOnly = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(875, 449);
            this.Controls.Add(this.chboxHeroType);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button1);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Guild Siege Log Reader";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.playerBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.guildBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.BindingSource guildBindingSource;
        private System.Windows.Forms.BindingSource playerBindingSource;
        private Utils.TextAndImageColumn Index;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn killsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pointsWithRPDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pointsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn playerNameListDataGridViewTextBoxColumn;
        private System.Windows.Forms.CheckBox chboxHeroType;
        private Utils.TextAndImageColumn playersIndex;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn GuildName;
        private System.Windows.Forms.DataGridViewTextBoxColumn PointsRp;
        private System.Windows.Forms.DataGridViewTextBoxColumn pointsDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn killsDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Deaths;
        private System.Windows.Forms.DataGridViewTextBoxColumn KD;
        private Utils.TextAndImageColumn textAndImageColumn1;
        private Utils.TextAndImageColumn textAndImageColumn2;
    }
}