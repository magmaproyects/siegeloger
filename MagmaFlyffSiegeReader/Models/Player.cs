﻿using System;

namespace MagmaFlyffSiegeReader.Models
{
    public class Player : IEquatable<Player>
    {
        private const int lifes = 12;
        public string Name { get; set; }
        public string GuildName { get; set; }
        public int Points { get; set; }
        public int PointsRp
        {
            get
            {
                return Points + (lifes - Deaths);
            }
        }
        public int Kills { get; set; }
        public int Deaths { get; set; }
        public Decimal KD
        {
            get
            {
                return Kills == 0 ? 0 : decimal.Round (Convert.ToDecimal( Kills) / Convert.ToDecimal( Deaths),2,MidpointRounding.AwayFromZero);
            }
        
        }

        public Player(string name, string guildName)
        {
            this.Name = name;
            this.GuildName = guildName;

        }
        public override int GetHashCode()
        {
            return GuildName.GetHashCode() + Name.GetHashCode();
        }

        public bool Equals(Player other)
        => this.Name == other.Name;
    }
}