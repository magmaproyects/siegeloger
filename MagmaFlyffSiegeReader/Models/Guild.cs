﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MagmaFlyffSiegeReader.Models
{
    public class Guild : IEquatable<Guild>
    {
        public string Name { get; set; }
        public int Points { get; set; }
        public int Kills { get; set; }
        public int PointsWithRP { get {
                return  Players.Sum(player => player.PointsRp);
            }
        }
        public ISet<Player> Players { get; set; }
        public string PlayerNameList
        {
            get
            {
                return string.Join(",",Players.Select(player => player.Name));
            }
        }

        public Guild(string name)
        {
            this.Name = name;
            this.Players = new HashSet<Player>();
        }
        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }


        public int GetHashCode(Guild obj)
        {
            throw new NotImplementedException();
        }

        public  bool Equals(Guild other)
        {
            return this.Name == other.Name;
        }
    }
}
