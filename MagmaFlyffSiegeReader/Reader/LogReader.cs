﻿using MagmaFlyffSiegeReader.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MagmaFlyffSiegeReader.Reader
{
  public class LogReader
    {
        private readonly string _route;
        public LogReader(string route)
        {
            _route = route;
        }
        public ISet<Guild> Siegeboard()
        {
            var guilds = new HashSet<Guild>();
            using (var sr = new StreamReader(_route))
            {
               
                while (!sr.EndOfStream)
                {

                    var peopleLine = sr.ReadLine();
                    var pointsline = sr.ReadLine();
                    LineReader lr = new LineReader(peopleLine, pointsline);

                    winGuild(guilds, lr);
                    loseGuild(guilds, lr);
                    var blankLine1 = sr.ReadLine();
                    var blankLine2 = sr.ReadLine();

                }
            }
            return guilds;
        }

        private static void winGuild(HashSet<Guild> guilds, LineReader lr)
        {
            var guild = lr.RetrieveGuildInfo().ElementAt(0);
            if (guilds.Contains(guild))
            {
                var winGuild = guilds.First(g => g.Name == guild.Name);
                winGuild.Kills += guild.Kills;
                winGuild.Points += guild.Points;
                if (winGuild.Players.Contains(guild.Players.ElementAt(0)))
                {
                    var winPlayer = winGuild.Players.First(p => p.Name == guild.Players.ElementAt(0).Name);
                    winPlayer.Kills += guild.Players.ElementAt(0).Kills;
                    winPlayer.Points += guild.Players.ElementAt(0).Points;
                }
                else
                {
                    winGuild.Players.Add(guild.Players.ElementAt(0));
                }
            }
            else
            {
                guilds.Add(guild);
            }
        }

        private static void loseGuild(HashSet<Guild> guilds, LineReader lr)
        {
            var guild = lr.RetrieveGuildInfo().ElementAt(1);
            if (guilds.Contains(guild))
            {
                var loseGuild = guilds.First(g => g.Name == guild.Name);
                if (loseGuild.Players.Contains(guild.Players.ElementAt(0)))
                {
                    var losePlayer = loseGuild.Players.First(p => p.Name == guild.Players.ElementAt(0).Name);
                    losePlayer.Deaths += 1;
                    
                }
                else
                {
                    loseGuild.Players.Add(guild.Players.ElementAt(0));
                }
            }
            else
            {
                guilds.Add(guild);
            }
        }
    }
}
