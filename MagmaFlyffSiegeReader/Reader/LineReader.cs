﻿using MagmaFlyffSiegeReader.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MagmaFlyffSiegeReader.Reader
{
    internal class LineReader
    {
        private string _peopleLine;
        private string _pointsLine;
        public LineReader(string peopleLine, string pointsLine)
        {
            this._peopleLine = peopleLine;
            this._pointsLine = pointsLine;
        }
        public ISet<Guild> RetrieveGuildInfo()
        {
            var peopleline = this._peopleLine.Replace("Guild Master","").Replace("Defender","").Split(' ');
            var pointsLine = this._pointsLine.Split(',');

            Guild winPointsGuild = new Guild(getGuildWinName(peopleline));
            winPointsGuild.Kills = 1;
            winPointsGuild.Points = getGuildWinPoints();
            winPointsGuild.Players.Add(getGuildWinPlayer(peopleline));


            Guild loseGuild = new Guild(getGuildLoseName(peopleline));
            loseGuild.Players.Add(getGuildLosePlayer(peopleline));
            var guilds = new HashSet<Guild>();
            guilds.Add(winPointsGuild);
            guilds.Add(loseGuild);
            return guilds;
        }

        private Player getGuildLosePlayer(string[] peopleline)

               => new Player(getLosePlayerName(peopleline), getGuildLoseName(peopleline));

        private string getLosePlayerName(string[] peopleline)
        {
            if (peopleline.Length >9)
            {
                return peopleline[9].Substring(0, peopleline[9].Length);
            }
            return peopleline[8].Substring(0, peopleline[8].Length);
        }

        private Player getGuildWinPlayer(string[] peopleline)
        {
            var player = new Player(getWinPlayerName(peopleline), getGuildWinName(peopleline));
            player.Kills = 1;
            player.Points = getGuildWinPoints();
            return player;
        }

        private string getWinPlayerName(string[] peopleline)
        {
            int index = 2;
            if (peopleline.Length > 10)
                index = 4;
            
            return peopleline[index].Substring(0, peopleline[index].Length - 2);
        }
        private string getGuildLoseName(string[] peopleline)
        {
            return peopleline[6].Replace("[", "").Replace("]", "");
        }

        private int getGuildWinPoints()
        {

            var allowedChars = "01234567890";
            return this._pointsLine.Where(c => allowedChars.Contains(c))
                .ToArray().Select(c => c.ToString()).ToArray()
                .Sum(x => int.Parse(x));

        }

        private string getGuildWinName(string[] peopleline)
        =>
             peopleline[0].Replace("[", "").Replace("]", "");
       
    }
}
